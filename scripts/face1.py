#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import cv2
import os
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import String
from time import sleep
path = os.path.dirname(os.path.abspath(__file__))
 
class Face_DeteCutter():
    def __init__(self):
        sub = rospy.Subscriber("/usb_cam/image_raw", Image, self.get_image)
        self.bridge = CvBridge()  # ROSとOpenCV間の通信
        self.image_org = None  # 画像
        self.image = None
        self.c_image = None
        cv2.startWindowThread()
 
    def get_image(self, img):
        try:
            self.image_org = self.bridge.imgmsg_to_cv2(img, "bgr8")  # 画像をOpenCVに変換
        except CvBridgeError, e:
            print e
 
    def face_save_cut(self):
        if self.image_org is None:  # 最初に実行
            return "画像データが格納されていません"
 
        org = self.image_org
        cv2.imshow('origenal',org)
        cv2.waitKey(1)
        gimg = cv2.cvtColor(org, cv2.COLOR_BGR2GRAY) #取得画像を濃淡化
 
        classifier = path + "/haarcascade_frontalface_alt2.xml"  # FaceDetectionData.xml
        cascade = cv2.CascadeClassifier(classifier)  # 顔識別画像をxmlファイルによって作成
 
        face = cascade.detectMultiScale(gimg, scaleFactor=1.2, minNeighbors=2, minSize=(10, 10))  # 顔識別処理
 
 
        if len(face) == 0:  # 顔画像が格納されない場合の処理
            return "顔検出ができませんでした"
        i = 0
        for rect in face:
            # 顔だけ切り出して保存
            x = rect[0]
            y = rect[1]
            width = rect[2]
            height = rect[3]
            self.c_image = org[y:y + height, x:x + width]
            self.c_image = cv2.resize(self.c_image, (50,50))

            cv2.imwrite(path + "/cut_face2/FaceCutImage" + str(i) + ".ppm", self.c_image)
            i += 1
        if len(face) > 0:
            color = (255, 255, 255)  # 枠の色
            for rect in face:
                cv2.rectangle(org, tuple(rect[0:2]),tuple(rect[0:2] + rect[2:4]), color, thickness=3)  # 顔データの計算
        cv2.imwrite(path + "/cut_face/Face.ppm", org)
            
        return '顔画像をディレクトリに保存しました'

def callback(message):
    rospy.loginfo("I hear " + message.data) #確認用
    fd = Face_DeteCutter()
    rate = rospy.Rate(1)
    num = 0
    while not rospy.is_shutdown():
        num = num + 1
        if num > 10:
            os.system('rosnode kill /Face_DeteCutter')
        log = fd.face_save_cut()
        rospy.loginfo(log)
        pub = rospy.Publisher("face", String, queue_size=10)
        if log == "顔画像をディレクトリに保存しました":
            string = String()
            string.data = "OK"
            pub.publish(string)
            rospy.loginfo("I send " + str(string.data))
            cv2.destroyAllWindows()
            rate.sleep()
            os.system('rosnode kill /Face_DeteCutter')
        rate.sleep()

if __name__ == "__main__":
    rospy.init_node("Face_DeteCutter")
    sub2 = rospy.Subscriber("/spr_state", String, callback)
    rospy.spin()
