#!/usr/bin/python env
# -*- coding: utf-8 -*-
import os
import numpy as np
import cv2
import pickle
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler


load_dir_pass = "face"

filenames = os.listdir(load_dir_pass) #フォルダにある画像を読み込み
num_files = len(filenames) #画像数を把握
data =  np.zeros((num_files,2500), np.uint8)
label = np.zeros((num_files),dtype='int32')
for i in range(num_files):
    filename = load_dir_pass + "/" + filenames[i]
    #データセット作成
    two_data = cv2.imread(filename, cv2.IMREAD_GRAYSCALE) #グレースケール
    one_data = two_data.flatten()
    data[i] = one_data #箱に入れていく
    #ラベル作成
    if filename.find("woman") > -1: #女
      label[i] = 1 
    else: #男
      label[i] = 0 

X = data
y = label
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=0)
print(X_train.shape)
print(X_test.shape)

#k-最近傍法
clf = KNeighborsClassifier(n_neighbors=3)
clf.fit(X_train, y_train)
print("Test set predictions: {}".format(clf.predict(X_test)))
print("Test set accuracy: {:.2f}".format(clf.score(X_test, y_test)))

#保存
save_filename = "finalized_model.sav"
pickle.dump(clf, open(save_filename, 'wb'))
