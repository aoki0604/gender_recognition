# 男女顔識別
## 使い方 
    $ rosrun usb_cam usb_cam_node
    $ rosrun gender_recognition face1.py
    $ rosrun gender_recognition face2.py
    $ rosrun gender_recognition spr_state_test.py
または、
    $ roslaunch gender_recognition gender_recognition.launch
    $ rosrun gender_recognition spr_state_test.py
で動くはず。


## ファイルの説明
face1.pyで顔を認識して、cut_face、cut_face2に保存。face2.pyでcut_face2にある写真を用いて男女識別。
face_learning.pyはfaceにある写真を用いて学習してfinalized_model.savを作るためのもの。faceには30×30のjpeg画像を入れている。

