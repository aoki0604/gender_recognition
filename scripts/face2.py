#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import cv2
import os
from std_msgs.msg import String
from time import sleep
import numpy as np
import pickle
from time import sleep
path = os.path.dirname(os.path.abspath(__file__))

def callback(message):
    rospy.loginfo("I hear " + message.data)
    read_filenames = os.listdir(path + "/cut_face2") #フォルダにある画像を読み込み
    read_num_files = len(read_filenames) #画像数を把握し

    if read_num_files == 0:
        return 0
    elif read_num_files == 1:
        read_filename = path + "/cut_face2/" + read_filenames[0]
        read_two_data = cv2.imread(read_filename, cv2.IMREAD_GRAYSCALE)
        read_data = read_two_data.flatten()
        read_data = read_data.reshape(1,-1)
    elif read_num_files > 1:
        read_data =  np.zeros((read_num_files,2500), np.uint8)  #画像データの箱を用意し
        #画像をひとつずつ読み込み箱に入れていく
        for i in range(read_num_files):
            read_filename = path + "/cut_face2/" + read_filenames[i]
            read_two_data = cv2.imread(read_filename, cv2.IMREAD_GRAYSCALE) #cv2で画像を読み込んで
            read_one_data = read_two_data.flatten()
            read_data[i] = read_one_data #箱に入れていく

    saved_filename = path + "/finalized_model.sav"
    loaded_model = pickle.load(open(saved_filename, 'rb'))
    ans = loaded_model.predict(read_data)
    woman = len(np.where(ans==1)[0])
    man = len(np.where(ans==0)[0])
    rospy.loginfo("peple:" + str(ans.size) + " man:" + str(man) + "  woman:" + str(woman))
    rate = rospy.Rate(1)  # 実行速度の調整
    rate.sleep()
    os.system("rosnode kill /Gender_Recognition")

if __name__ == '__main__':
    sub = rospy.Subscriber("face", String, callback)
    rospy.init_node("Gender_Recognition")
    rospy.spin()
